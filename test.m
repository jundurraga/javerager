close all;
fs=56000;
% time = (0:1/fs:0.02)*1000;
time = (0:2^8-1)*1/fs*1000;
clear jave jave2;
jave=JAverager;
jave.Fs = fs;
jave.TimeOffset = 0e-3;
jave.AnalysisWindow = [];
jave.FilterWindow = []*1e-3;
jave.Blanking = []*1e-3;
jave.RejectionLevel = inf;
jave.Splits = 2;
jave.tPSNR = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8]*1e-3;
jave.AlphaLevel = 0.05;
jave.MinBlockSize = 32;
jave.PlotSteps = 0;
jave.RejectionWindow = []*1e-3;
jave.FilterHP = [];
jave.FilterLP = [];
jave.NSampTeoResNoise = 32;
jave.TypeAverage = 1;
jave.SplitsByID = false;
jave.FFTFrequencies = [1000];
jave.FFTAnalysis = true;
% jave.WindowRN = [2,2.2]*1e-3;
%signal = generateTargetSignal('Time',time,'APPosW', [0.25/2,2,1;-0.25/2,1,1])';
signal = zeros(size(time))';
f = 1000;
cycles = 1.5/f*fs;
signal(1:cycles) = -sin(2*pi*f*(0:cycles-1)/fs-0*pi/2).*exp(-(0:cycles-1)/fs/0.0005);
signal = signal - mean(signal,1);
stdSignal= std(signal);
N = 12800;
desiredSNR = 10;
inistd = 10^(-desiredSNR/20)*stdSignal*N^0.5;
TheorycalRN = inistd/N^0.5;
%ideal white noise filtered output
% TheorycalRNFiltered = inistd*sqrt(2*(6000 - 400)/fs) /N^0.5;
% NewSource = 12800;
for i = 1:N
%     noise = -(i<2560)*generateEABRNoise('Time',time,...
%         'Index',i,...
%         'NewSource',NewSource,...
%         'SigmaNewSource',inistd*((1+2*floor((i/NewSource)))),...
%         'Slope',0,...
%         'Bias',0,...
%         'Tau',0.0058);
%     cnoise = generateNoise('Fs',fs,'T',round(numel(signal)/fs),'Attenuation',0,'ShowSpectrogram',0,'PlotResults',0);
%     sdnoise = std(cnoise);
%     noise = inistd/sdnoise * cnoise;
%     noise =  inistd*randn(numel(signal),1);
%     noise = flipdim(noise,1);
%     jave.CurrentSplitID = mod(i-1,3)+1;
%     jave.AddSweep(noise(1:numel(signal))*(mod(i-1,3)>0) + (mod(i-1,3)<=1)*signal);
%     jave.AddSweep(signal + 0*noise(1:numel(signal))*(mod(i-1,3)>0));
    noise =  (1 + .001*(i>1000)*(i<6000)) * inistd*randn(numel(signal),1);
    jave.AddSweep(noise(1:numel(signal)) + signal);
end
% javeragerExplorer('JAveragerArray',jave)
% plot([signal,jave.SAverage,...
%     jave.SAverage+(jave.SAverageVar).^0.5,...
%     jave.SAverage-(jave.SAverageVar).^0.5]);
% 
% figure;
% plot([signal,jave.SAverage,...
%     jave.SAverage+0.15e-6/(N^0.5),...
%     jave.SAverage-0.15e-6/(N^0.5)]);
% x = frnd(2,20,1000,1);
jave.plotNoise
% mle(jave.Average,'pdf',@fpdf,'start',1:2)
% disp([TheorycalRN,TheorycalRNFiltered,jave.RN])
jave.TypeAverage =1; disp(jave.SNR); jave.TypeAverage = 2;disp(jave.SNR)