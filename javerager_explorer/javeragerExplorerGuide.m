function varargout = javeragerExplorerGuide(varargin)
% JAVERAGEREXPLORERGUIDE M-file for javeragerExplorerGuide.fig
%      JAVERAGEREXPLORERGUIDE, by itself, creates a new JAVERAGEREXPLORERGUIDE or raises the existing
%      singleton*.
%
%      H = JAVERAGEREXPLORERGUIDE returns the handle to a new JAVERAGEREXPLORERGUIDE or the handle to
%      the existing singleton*.
%
%      JAVERAGEREXPLORERGUIDE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in JAVERAGEREXPLORERGUIDE.M with the given input arguments.
%
%      JAVERAGEREXPLORERGUIDE('Property','Value',...) creates a new JAVERAGEREXPLORERGUIDE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before javeragerExplorerGuide_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to javeragerExplorerGuide_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help javeragerExplorerGuide

% Last Modified by GUIDE v2.5 19-Oct-2011 17:48:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @javeragerExplorerGuide_OpeningFcn, ...
                   'gui_OutputFcn',  @javeragerExplorerGuide_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before javeragerExplorerGuide is made visible.
function javeragerExplorerGuide_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to javeragerExplorerGuide (see VARARGIN)

% Choose default command line output for javeragerExplorerGuide
handles.output = hObject;
handles.JAveragerArray = [];
handles.DescriptorArray = '';
handles.AddJaverager = @callback_AddJAverager;

%Color gui
brColor = [170,202,230]/256;
set(0,'defaultUicontrolBackgroundColor',brColor);
hbc = findobj('-property','BackgroundColor');
set(hbc,'BackgroundColor',brColor);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes javeragerExplorerGuide wait for user response (see UIRESUME)
% uiwait(handles.figure1);
function callback_AddJAverager(varargin)
    s = parseparameters(varargin{:});
    s = ef(s,'JAveragers',[]);
    s = ef(s,'hObject',[]);
    s = ef(s,'Descriptors','');
    handles = guidata(s.hObject);
    handles.JAveragerArray = s.JAveragers;
    handles.DescriptorArray = s.Descriptors;
    guidata(s.hObject,handles);
    updatePopData(s.hObject);
    updateProperties(s.hObject);
    
    
function updatePopData(hObject)
    handles = guidata(hObject);
    for i = 1: numel(handles.JAveragerArray)
        data(i) = handles.JAveragerArray(i);
        if ~isempty(handles.DescriptorArray)
            descriptor{i} = [num2str(i),'-',handles.DescriptorArray{i}];
        else
            descriptor{i} = [num2str(i)];
        end
    end
    set(handles.pupData,'String',descriptor);
    

% --- Outputs from this function are returned to the command line.
function varargout = javeragerExplorerGuide_OutputFcn(hObject, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in pupData.
function pupData_Callback(hObject, ~, handles)
   updateProperties(hObject);

function updateProperties(hObject)
    handles = guidata(hObject);
    text = struct2text(struct( handles.JAveragerArray(get(handles.pupData,'Value')))); 
    set(handles.lbProperties,'String',text);

% --- Executes during object creation, after setting all properties.
function pupData_CreateFcn(hObject, ~, handles)
% hObject    handle to pupData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbHistogram.
function pbHistogram_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).histogramsPlot;


% --- Executes on button press in pbCDF.
function pbCDF_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).cdfPlot;

% --- Executes on button press in pbProbability.
function pbProbability_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).probabilityPlot;
% --- Executes on button press in pbNoisePlot.

function pbNoisePlot_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).plotNoise;

function setAverager(hObject);
    handles = guidata(hObject);
    switch get(handles.cbWave,'Value')
        case 1 
            typeAve = 1;
        case 0 
            typeAve = 2;
        otherwise
    end
    for i = 1: numel(handles.JAveragerArray)
        handles.JAveragerArray(i).TypeAverage = typeAve;
        handles.JAveragerArray(i).MixTrackedPoints = get(handles.chMixTrackedPoints,'Value');
    end
   
    
% --- Executes on button press in cbWave.
function cbWave_Callback(hObject, ~, handles)
% hObject    handle to cbWave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbWave


% --- Executes on button press in chMixTrackedPoints.
function chMixTrackedPoints_Callback(hObject, ~, handles)
% hObject    handle to chMixTrackedPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chMixTrackedPoints


% --- Executes on selection change in lbProperties.
function lbProperties_Callback(hObject, ~, handles)
% hObject    handle to lbProperties (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns lbProperties contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lbProperties


% --- Executes during object creation, after setting all properties.
function lbProperties_CreateFcn(hObject, ~, handles)
% hObject    handle to lbProperties (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbFFT.
function pbFFT_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).fftPlot;


% --- Executes on button press in pbAverage.
function pbAverage_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).averagePlot;


% --- Executes on button press in pbFTestFitting.
function pbFTestFitting_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).FTestFitting;


% --- Executes on button press in btPSD.
function btPSD_Callback(hObject, ~, handles)
	setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).psdPlot;


% --- Executes on button press in pbExponentialFit.
function pbExponentialFit_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).exponentialFitPlot;


% --- Executes on button press in pbTrackedPointsDf.
function pbTrackedPointsDf_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).TrackedPointsDf;


% --- Executes on button press in pbCorrTrackedPoints.
function pbCorrTrackedPoints_Callback(hObject, ~, handles)
    setAverager(hObject);
    handles.JAveragerArray(get(handles.pupData,'Value')).CorrelationTrackedPoints;


% --- Executes on button press in pbExportData.
function pbExportData_Callback(hObject, ~, handles)
    setAverager(hObject);
    ctext = struct2text(struct( handles.JAveragerArray(get(handles.pupData,'Value')))); 
    for i=1:numel(ctext)    
        listValues(i) = regexp(ctext(i),'\:*\s*','split');
        cStruct.(listValues{i}{1}) = listValues{i}{2};
    end
    tableGenerator('StructureParams',cStruct);