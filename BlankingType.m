classdef  BlankingType
properties  (Constant)
    Interpolation = 1;
    ZeroPadding = 2;
    RepeatLastVal = 3;
end %constant properties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
methods (Access = private)
%private so that you can't instatiate.
    function out = BlankingType
    end %Colors()
end %private methods
end %class Colors
